#!/bin/bash
clear
echo "Limpiando los directorios"
hdfs dfs -rm -R /data/proyecto_final/*
echo "Creando Directorios"
hdfs dfs -mkdir /data/proyecto_final/raw
hdfs dfs -put PRSA_data_2010.1.1-2014.12.31.csv /data/proyecto_final/raw/prsa.csv
hdfs dfs -du -h /data/proyecto_final/*
echo "Creando la BD"
hive -f Proyecto.hql
spark-submit Proyecto.py --driver-memory 3G
