SET hive.exec.dynamic.partition = true;
SET hive.exec.dynamic.partition.mode = nonstrict;
SET hive.exec.max.dynamic.partitions.pernode = 40000;
SET parquet.block.size = 1073741824;

DROP TABLE proyecto_raw;
DROP TABLE proyecto;
DROP TABLE proyecto_min_max;

CREATE EXTERNAL TABLE `proyecto_raw` (`no` int, `year` int, `month` int, `day` int, `hour` int, `pm25` string, `dewp` string, `temp` string, `pres` string, `cbwd` string, `iws` string, `is` string, `ir` string) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' STORED AS TEXTFILE LOCATION '/data/proyecto_final/raw';

SELECT * FROM proyecto_raw limit 10;

CREATE EXTERNAL TABLE `proyecto` (`no` int, `date` timestamp, `Pollution` int, `Dew` int, `Temp` double, `Press` double, `Wnd_dir` string, `Wnd_spd` double, `Snow` int, `Rain` int) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' STORED AS TEXTFILE LOCATION '/data/proyecto_final/textfile';

INSERT INTO proyecto SELECT no, cast(concat(year, '-', lpad(month,2,'0'), '-', lpad(day,2,'0'), ' ', lpad(hour,2,'0'), ':00:00') as timestamp), if(pm25='NA', 0, pm25), dewp, temp, pres, cbwd, iws, is, ir from proyecto_raw WHERE no is not null;

SELECT * FROM proyecto where date is not null limit 10;

CREATE EXTERNAL TABLE `proyecto_min_max` (`Pollution_min` int, `Pollution_max` int,  `Dew_min` int,  `Dew_max` int, `Temp_min` double, `Temp_max` double, `Press_min` double, `Press_max` double, `Wnd_spd_min` double, `Wnd_spd_max` double, `Snow_min` int, `Snow_max` int, `Rain_min` int, `Rain_max` int) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' STORED AS TEXTFILE LOCATION '/data/proyecto_final/minmax';

INSERT INTO proyecto_min_max select min(Pollution), max(Pollution), min(Dew), max(Dew), min(Temp), max(Temp), min(Press), max(Press), min(Wnd_spd), max(Wnd_spd), min(Snow), max(Snow), min(Rain), max(Rain) from proyecto;

SELECT * FROM proyecto_min_max;
