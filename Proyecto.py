import os
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext, HiveContext

# Configure the environment
if 'SPARK_HOME' not in os.environ:
    os.environ['SPARK_HOME'] = '/usr/lib/spark'

#conf = SparkConf().setAppName('proyectofinal').setMaster('local[32]')
conf = SparkConf().setAppName('proyectofinal')
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)
print('Nivel de log ERROR')
sc.setLogLevel('ERROR')
print('\n\n\n\n\n\n\n\n')

print('Probando conexion')
sqlContext.sql("show tables").show()
print('\n\n\n\n\n\n\n\n')

print('Borrando Tablas')
sqlContext.sql('drop table Beijin_norm_incomplete')
sqlContext.sql('drop table Beijin_norm')
sqlContext.sql('drop table Beijin_prom')
print('\n\n\n\n\n\n\n\n')

print('Creando el dataFrame de proyecto')
dataFrame = sqlContext.sql('SELECT * FROM proyecto')
dataFrame.show()
dataFrame.printSchema()
listaOriginal = dataFrame.rdd.map(list).collect()
print('\n\n\n\n\n\n\n\n')

print('Creando el dataFrame de proyecto_min_max')
dataFrameMinMax = sqlContext.sql('SELECT * FROM proyecto_min_max')
dataFrameMinMax.show()
dataFrameMinMax.printSchema()
print('\n\n\n\n\n\n\n\n')

print('Creando la lista de min max')
listaMinMax = dataFrameMinMax.rdd.map(lambda x: [x.pollution_min, x.pollution_max, x.dew_min, x.dew_max, x.temp_min, x.temp_max, x.press_min, x.press_max, x.wnd_spd_min, x.wnd_spd_max, x.snow_min, x.snow_max, x.rain_min, x.rain_max]).collect()[0]
print(listaMinMax)
print('\n\n\n\n\n\n\n\n')

print('Creando la normalizacion por registro')
#rdd_normalizado = dataFrame.rdd.map(lambda row: [row.no, row.date, abs((row.pollution - listaMinMax[0]) / (listaMinMax[0] - listaMinMax[1])), abs((row.dew - listaMinMax[2]) / (listaMinMax[2] - listaMinMax[3])), abs((row.temp - listaMinMax[4]) / (listaMinMax[4] - listaMinMax[5])), abs((row.press - listaMinMax[6]) / (listaMinMax[6] - listaMinMax[7])), abs((row.wnd_spd - listaMinMax[8]) / (listaMinMax[8] - listaMinMax[9])), abs((row.snow - listaMinMax[10]) / (listaMinMax[10] - listaMinMax[11])), abs((row.rain - listaMinMax[12]) / (listaMinMax[12] - listaMinMax[13]))])
rdd_normalizado = dataFrame.select(['no', 'date','pollution','dew','temp','press','wnd_spd','snow','rain']).rdd.map(lambda row: [row.no, row.date, (row.pollution - min(row[2:8:]))/(max(row[2:8:])-min(row[2:8:])), (row.dew - min(row[2:8:]))/(max(row[2:8:])-min(row[2:8:])), (row.temp - min(row[2:8:]))/(max(row[2:8:])-min(row[2:8:])), (row.press - min(row[2:8:]))/(max(row[2:8:])-min(row[2:8:])), (row.wnd_spd - min(row[2:8:]))/(max(row[2:8:])-min(row[2:8:])), (row.snow - min(row[2:8:]))/(max(row[2:8:])-min(row[2:8:])), (row.rain - min(row[2:8:]))/(max(row[2:8:])-min(row[2:8:]))])
#rdd_lista = rdd_normalizado.collect()
#print(rdd_lista)
print('\n\n\n\n\n\n\n\n')

print('Creando rdd normalizado incompleto')
df_normalizado = rdd_normalizado.toDF(['no', 'date', 'Pollution_norm', 'Dew_norm', 'Temp_norm', 'Press_norm', 'Wnd_spd_norm', 'Snow_norm', 'Rain_norm'])
df_normalizado.show()
df_normalizado.printSchema()
print('\n\n\n\n\n\n\n\n')

print('Guardando en hivel rdd normalizado incompleto Beijin_norm_incomplete')
df_normalizado.write.saveAsTable('Beijin_norm_incomplete')
beijinNormIncomplete = sqlContext.sql('SELECT * FROM Beijin_norm_incomplete')
beijinNormIncomplete.show()
beijinNormIncomplete.printSchema()
print('\n\n\n\n\n\n\n\n')

print('Creando la tabla original mas normalizada')
df_beijinNorm = sqlContext.sql('SELECT DATE_FORMAT(DATE(proyecto.date), "yyyy-MM") corto, proyecto.date, proyecto.Pollution, proyecto.Dew, proyecto.Temp, proyecto.Press, proyecto.Wnd_dir, proyecto.Wnd_spd, proyecto.Snow, proyecto.Rain, incomplete.date as datePlus, incomplete.Pollution_norm, incomplete.Dew_norm, incomplete.Temp_norm, incomplete.Press_norm, incomplete.Wnd_spd_norm, incomplete.Snow_norm, incomplete.Rain_norm FROM Beijin_norm_incomplete incomplete JOIN proyecto on proyecto.no = incomplete.no ')
df_beijinNorm.show()
print('\n\n\n\n\n\n\n\n')

print('Guardando la tabla en hive Beijin_norm')
df_beijinNorm.write.saveAsTable('Beijin_norm')
beijinNorm = sqlContext.sql('SELECT * FROM Beijin_norm')
beijinNorm.show()
beijinNorm.printSchema()
print('\n\n\n\n\n\n\n\n')

print('Creando DataFrame del mes')
df_normalizado_mes = beijinNorm.select(['corto','Pollution','Dew','Temp','Press','Wnd_dir','Wnd_spd','Snow','Rain']).groupBy('corto').avg().withColumnRenamed('avg(Pollution)', 'Pollution').withColumnRenamed('avg(Dew)', 'Dew').withColumnRenamed('avg(Temp)', 'Temp').withColumnRenamed('avg(Press)', 'Press').withColumnRenamed('avg(Wnd_dir)', 'Wnd_dir').withColumnRenamed('avg(Wnd_spd)', 'Wnd_spd').withColumnRenamed('avg(Snow)', 'Snow').withColumnRenamed('avg(Rain)', 'Rain');
df_normalizado_mes.show()
df_normalizado_mes.printSchema()
print('\n\n\n\n\n\n\n\n')

print('Creando la tabla Beijin_prom en hive')
df_normalizado_mes.write.saveAsTable('Beijin_prom')
beijinProm = sqlContext.sql('SELECT * FROM Beijin_prom')
beijinProm.show()
beijinProm.printSchema()
print('\n\n\n\n\n\n\n\n')
